#!/bin/bash

CONFIG_FILE=config.mak
MAKE_FILE=Makefile

# Default configure for config.mak
CC=gcc
AR=ar
CPP="gcc -E"
RM='rm -f'
CFLAGS=
LDFLAGS=
DISABLE_DEBUG=no
DISABLE_CASSERT=no
HAVE_EPOLL=
HAVE_KQUEUE=
HAVE_SELECT=
HAVE_SELECT=
TARGET_SYSTEM=
BUILD_SYSTEM=

# Default configure for Makefile
PREFIX='/usr/local'
INCLUDEDIR=

# Parse command line options
usage() {
	cat<<EOT
./configure [OPTIONS] ... [VAR=VALUE] ...

Options:
  -h, --help              display this help and exit
  --prefix                install architecture-independent files in PREFIX
                          [/usr/local]
  --includedir=DIR        C header files [PREFIX/include]
  --with-pic              use only PIC/non-PIC objects [default=no]
  --disable-debug         disable support for running in debug mode
  --disable-cassert       disable C assertion

Some influential environment variables:
  CC          C compiler command
  CFLAGS      C compiler flags
  LDFLAGS     linker flags, e.g. -L<lib dir> if you have libraries in a
              nonstandard directory <lib dir>
EOT
}

TEMP=`getopt -o h --long help,prefix:,includedir:,with-pic,disable-debug,disable-cassert -n 'configure' -- "$@"`

if [ $? != 0 ] ; then
	usage
	exit 1
fi

eval set -- "$TEMP"

while true; do
	case "$1" in
		-h|--help) usage; exit 0;;
		--prefix) PREFIX=$2; shift 2;;
		--includedir) INCLUDEDIR=$2; shift 2;;
		--with-pic) CFLAGS="$CLFAGS -fPIC"; shift;;
		--disable-debug) DISABLE_DEBUG=yes; shift;;
		--disable-cassert) DISABLE_CASSERT=yes; shift;;
		--) shift; break;;
	esac
done

if [ -z "$INCLUDEDIR" ]; then
	INCLUDEDIR="$PREFIX/include"
fi

ORIG_IFS="$IFS"
IFS="="
for arg do
	PAIR=($arg)
	case ${PAIR[0]} in
		CC) CC=${PAIR[1]};CPP="$CC -E";;
		CFLAGS) CFLAGS="$CFLAGS ${PAIR[1]}";;
		LDFLAGS) LDFLAGS="$LDFLAGS ${PAIR[1]}";;
	esac
done
IFS="$ORIG_IFS"

# Test build conditions

TEST_CFLAGS="-Wall -Werror -O0"
TEST_CC=$CC
TEST_FILE=test.c
TEST_FILE_OUTPUT=test.o

compile-test() {
	local TEST_NAME=$1
	local CONFIG_NAME=$2
	local RESULT=no
	local RET_CODE=1
	echo -e "checking for $TEST_NAME... \c" 

	cat > $TEST_FILE
	$TEST_CC $TEST_CFLAGS -c $TEST_FILE -o $TEST_FILE_OUTPUT > /dev/null 2>&1

	if [ $? -eq 0 ]; then
		RET_CODE=0
		RESULT=yes
	fi
	echo $RESULT
	eval $CONFIG_NAME=$RESULT
	return $RET_CODE
}

macro-defined-test() {
	local MACRO_NAME=$1
	local CONFIG_NAME=$2
	local RESULT=no
	echo -e "checking whether $MACRO_NAME defined... \c" 
	cat > $TEST_FILE
	cat >> $TEST_FILE <<EOT
#ifdef $MACRO_NAME
@TEST_RESULT yes
#else
@TEST_RESULT no
#endif
EOT
	RESULT=$($CPP $TEST_FILE | sed -ne '/@TEST_RESULT/p' | sed -e 's/^@TEST_RESULT \(.*\)$/\1/')
	echo $RESULT
	eval $CONFIG_NAME=$RESULT
	return 0
}

macro-test() {
	local MACRO_NAME=$1
	local CONFIG_NAME=$2
	local RESULT=no
	echo -e "checking for macro $MACRO_NAME... \c" 
	cat > $TEST_FILE
	echo "@TEST_RESULT $MACRO_NAME" >> $TEST_FILE
	RESULT=$($CPP $TEST_FILE | sed -ne '/@TEST_RESULT/p' | sed -e 's/^@TEST_RESULT \(.*\)$/\1/')
	echo $RESULT
	eval $CONFIG_NAME=$RESULT
	return 0
}

build-system-test() {
	local CONFIG_NAME=$1
	local RESULT=
	echo -e "checking for system for build... \c" 
	if [ "$OS" == "Windows_NT" ]; then
		RESULT=win32
	else
		RESULT=$(echo `uname -s` | sed 's/.*/\L&/')
	fi
	echo $RESULT
	eval $CONFIG_NAME=$RESULT
}

target-system-test() {
	local CONFIG_NAME=$1
	local RESULT=
	echo -e "checking for system for target... \c" 
	cat include/ax/detect.h > $TEST_FILE
	echo 'echo AX_OS_TEXT' >> $TEST_FILE
	RESULT=$($CPP $TEST_FILE | sh)
	echo $RESULT
	eval $CONFIG_NAME=$RESULT
	# gcc -E -dM -xc -c 
}

build-system-test BUILD_SYSTEM

target-system-test TARGET_SYSTEM

compile-test select HAVE_SELECT <<EOT
#ifdef WIN32
#include <winsock.h>
#else
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#endif
int main() {
    select(0, NULL, NULL, NULL, NULL);
    return 0;
}
EOT

compile-test poll HAVE_POLL <<EOT
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/poll.h>
int main() {
    poll(NULL, 0, 0);
    return 0;
}
EOT

compile-test epoll HAVE_EPOLL <<EOT
#ifdef WIN32
int main() { }
#else
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/epoll.h>
int main() {
    epoll_wait(0, NULL, 0, -1);
    return 0;
}
#endif
EOT

compile-test kqueue HAVE_KQUEUE <<EOT
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>

int main() {
    (void)kqueue();
    return 0;
}
EOT

compile-test afunix.h HAVE_AFUNIX_H <<EOT
#include <winsock2.h>
#include <afunix.h>
int main() {
    (void)sizeof(struct sockaddr_un);
    return 0;
}
EOT

$RM $TEST_FILE $TEST_FILE_OUTPUT

# Apply configure

sed -e "s#@CC@#$CC#g" \
    -e "s#@AR@#$AR#g" \
    -e "s#@RM@#$RM#g" \
    -e "s#@CFLAGS@#$CFLAGS#g" \
    -e "s#@LDFLAGS@#$LDFLAGS#g" \
    -e "s#@DISABLE_DEBUG@#$DISABLE_DEBUG#g" \
    -e "s#@DISABLE_CASSERT@#$DISABLE_CASSERT#g" \
    -e "s#@HAVE_POLL@#$HAVE_POLL#g" \
    -e "s#@HAVE_EPOLL@#$HAVE_EPOLL#g" \
    -e "s#@HAVE_KQUEUE@#$HAVE_KQUEUE#g" \
    -e "s#@HAVE_SELECT@#$HAVE_SELECT#g" \
    -e "s#@HAVE_AFUNIX_H@#$HAVE_AFUNIX_H#g" \
    -e "s#@BUILD_SYSTEM@#$BUILD_SYSTEM#g" \
    -e "s#@TARGET_SYSTEM@#$TARGET_SYSTEM#g" \
    > $CONFIG_FILE < $CONFIG_FILE.in

sed -e "s#@PREFIX@#$PREFIX#g" \
    -e "s#@INCLUDEDIR@#$INCLUDEDIR#g" \
    > $MAKE_FILE < $MAKE_FILE.in

